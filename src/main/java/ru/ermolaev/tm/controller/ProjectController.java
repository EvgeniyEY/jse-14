package ru.ermolaev.tm.controller;

import ru.ermolaev.tm.api.controller.IProjectController;
import ru.ermolaev.tm.api.service.IAuthenticationService;
import ru.ermolaev.tm.api.service.IProjectService;
import ru.ermolaev.tm.model.Project;
import ru.ermolaev.tm.util.TerminalUtil;

import java.util.List;

public class ProjectController implements IProjectController {

    private final IProjectService projectService;

    private final IAuthenticationService authenticationService;

    public ProjectController(
            final IProjectService projectService,
            final IAuthenticationService authenticationService
    ) {
        this.projectService = projectService;
        this.authenticationService = authenticationService;
    }

    @Override
    public void showProjects() throws Exception {
        System.out.println("[PROJECTS LIST]");
        final String userId = authenticationService.getUserId();
        final List<Project> projects = projectService.showAllProjects(userId);
        int index = 1;
        for (Project project: projects) {
            System.out.println(index + ". " + project);
            index++;
        }
        System.out.println("[COMPLETE]");
    }

    @Override
    public void clearProjects() throws Exception {
        System.out.println("[CLEAR PROJECTS]");
        final String userId = authenticationService.getUserId();
        projectService.removeAllProjects(userId);
        System.out.println("[COMPLETE]");
    }

    @Override
    public void createProject() throws Exception {
        System.out.println("[CREATE PROJECT]");
        System.out.println("ENTER PROJECT NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER PROJECT DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final String userId = authenticationService.getUserId();
        projectService.createProject(userId, name, description);
        System.out.println("[COMPLETE]");
    }

    @Override
    public void showProjectById() throws Exception {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        final String userId = authenticationService.getUserId();
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findProjectById(userId, id);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        showProject(project);
        System.out.println("[COMPLETE]");
    }

    @Override
    public void showProjectByIndex() throws Exception {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER PROJECT INDEX:");
        final String userId = authenticationService.getUserId();
        final Integer index = TerminalUtil.nextNumber() -1;
        final Project project = projectService.findProjectByIndex(userId, index);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        showProject(project);
        System.out.println("[COMPLETE]");
    }

    @Override
    public void showProjectByName() throws Exception {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER PROJECT NAME:");
        final String userId = authenticationService.getUserId();
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.findProjectByName(userId, name);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        showProject(project);
        System.out.println("[COMPLETE]");
    }

    @Override
    public void updateProjectById() throws Exception {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        final String userId = authenticationService.getUserId();
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findProjectById(userId, id);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER NEW PROJECT NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER NEW PROJECT DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdated = projectService.updateProjectById(userId, id, name, description);
        if (projectUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[COMPLETE]");
    }

    @Override
    public void updateProjectByIndex() throws Exception {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("ENTER PROJECT INDEX:");
        final String userId = authenticationService.getUserId();
        final Integer index = TerminalUtil.nextNumber() -1;
        final Project project = projectService.findProjectByIndex(userId, index);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER NEW PROJECT NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER NEW PROJECT DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdated = projectService.updateProjectByIndex(userId, index, name, description);
        if (projectUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[COMPLETE]");
    }

    @Override
    public void removeProjectById() throws Exception {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        final String userId = authenticationService.getUserId();
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.removeProjectById(userId, id);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[COMPLETE]");
    }

    @Override
    public void removeProjectByIndex() throws Exception {
        System.out.println("[REMOVE PROJECT]");
        final String userId = authenticationService.getUserId();
        System.out.println("ENTER PROJECT INDEX:");
        final Integer index = TerminalUtil.nextNumber() -1;
        final Project project = projectService.removeProjectByIndex(userId, index);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[COMPLETE]");
    }

    @Override
    public void removeProjectByName() throws Exception {
        System.out.println("[REMOVE PROJECT]");
        final String userId = authenticationService.getUserId();
        System.out.println("ENTER PROJECT NAME:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.removeProjectByName(userId, name);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[COMPLETE]");
    }

    private void showProject(final Project project) {
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
    }

}
