package ru.ermolaev.tm.controller;

import ru.ermolaev.tm.api.controller.ITaskController;
import ru.ermolaev.tm.api.service.IAuthenticationService;
import ru.ermolaev.tm.api.service.ITaskService;
import ru.ermolaev.tm.model.Task;
import ru.ermolaev.tm.util.TerminalUtil;

import java.util.List;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    private final IAuthenticationService authenticationService;

    public TaskController(
            final ITaskService taskService,
            final IAuthenticationService authenticationService
    ) {
        this.taskService = taskService;
        this.authenticationService = authenticationService;
    }

    @Override
    public void showTasks() throws Exception {
        System.out.println("[TASK LIST]");
        final String userId = authenticationService.getUserId();
        final List<Task> tasks = taskService.showAllTasks(userId);
        for (Task task: tasks) {
            System.out.println((tasks.indexOf(task) + 1) + ". " + task);
        }
        System.out.println("[COMPLETE]");
    }

    @Override
    public void clearTasks() throws Exception {
        System.out.println("[CLEAR TASKS]");
        final String userId = authenticationService.getUserId();
        taskService.removeAllTasks(userId);
        System.out.println("[COMPLETE]");
    }

    @Override
    public void createTask() throws Exception {
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER TASK NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER TASK DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final String userId = authenticationService.getUserId();
        taskService.createTask(userId, name, description);
        System.out.println("[COMPLETE]");
    }

    @Override
    public void showTaskById() throws Exception {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER TASK ID:");
        final String userId = authenticationService.getUserId();
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findTaskById(userId, id);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        showTask(task);
        System.out.println("[COMPLETE]");
    }

    @Override
    public void showTaskByIndex() throws Exception {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER TASK INDEX:");
        final String userId = authenticationService.getUserId();
        final Integer index = TerminalUtil.nextNumber() -1;
        final Task task = taskService.findTaskByIndex(userId, index);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        showTask(task);
        System.out.println("[COMPLETE]");
    }

    @Override
    public void showTaskByName() throws Exception {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER TASK NAME:");
        final String userId = authenticationService.getUserId();
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.findTaskByName(userId, name);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        showTask(task);
        System.out.println("[COMPLETE]");
    }

    @Override
    public void updateTaskById() throws Exception {
        System.out.println("[UPDATE TASK]");
        System.out.println("ENTER TASK ID:");
        final String userId = authenticationService.getUserId();
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findTaskById(userId, id);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER NEW TASK NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER NEW TASK DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdated = taskService.updateTaskById(userId, id, name, description);
        if (taskUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[COMPLETE]");
    }

    @Override
    public void updateTaskByIndex() throws Exception {
        System.out.println("[UPDATE TASK]");
        System.out.println("ENTER TASK INDEX:");
        final String userId = authenticationService.getUserId();
        final Integer index = TerminalUtil.nextNumber() -1;
        final Task task = taskService.findTaskByIndex(userId, index);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER NEW TASK NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER NEW TASK DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdated = taskService.updateTaskByIndex(userId, index, name, description);
        if (taskUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[COMPLETE]");
    }

    @Override
    public void removeTaskById() throws Exception {
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER TASK ID:");
        final String userId = authenticationService.getUserId();
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.removeTaskById(userId, id);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[COMPLETE]");
    }

    @Override
    public void removeTaskByIndex() throws Exception {
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER TASK INDEX:");
        final String userId = authenticationService.getUserId();
        final Integer index = TerminalUtil.nextNumber() -1;
        final Task task = taskService.removeTaskByIndex(userId, index);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[COMPLETE]");
    }

    @Override
    public void removeTaskByName() throws Exception {
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER TASK NAME:");
        final String userId = authenticationService.getUserId();
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.removeTaskByName(userId, name);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[COMPLETE]");
    }

    private void showTask(final Task task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
    }

}
