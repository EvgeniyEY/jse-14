package ru.ermolaev.tm.controller;

import ru.ermolaev.tm.api.controller.IAuthenticationController;
import ru.ermolaev.tm.api.service.IAuthenticationService;
import ru.ermolaev.tm.model.User;
import ru.ermolaev.tm.util.TerminalUtil;

public class AuthenticationController implements IAuthenticationController {

    private IAuthenticationService authenticationService;

    public AuthenticationController(final IAuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    @Override
    public void login() throws Exception {
        System.out.println("[LOGIN]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        authenticationService.login(login, password);
        System.out.println("[OK]");
    }

    @Override
    public void logout() {
        System.out.println("[LOGOUT]");
        authenticationService.logout();
        System.out.println("[OK]");
    }

    @Override
    public void registration() throws Exception {
        System.out.println("[REGISTRATION]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        System.out.println("ENTER E-MAIL:");
        final String email = TerminalUtil.nextLine();
        authenticationService.registration(login, password, email);
        System.out.println("[OK]");
    }

    @Override
    public void updatePassword() throws Exception {
        System.out.println("[CHANGE PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        final String newPassword = TerminalUtil.nextLine();
        authenticationService.updatePassword(newPassword);
        System.out.println("[PASSWORD CHANGED]");
        logout();
        System.out.println("[ENTER IN YOUR ACCOUNT AGAIN]");
    }

    @Override
    public void showUserProfile() throws Exception {
        System.out.println("[SHOW USER PROFILE]");
        final User user = authenticationService.showUserProfile();
        System.out.println(user);
        System.out.println("[OK]");
    }

    @Override
    public void updateUserFirstName() throws Exception {
        System.out.println("[UPDATE USER FIRST NAME]");
        System.out.println("ENTER NEW USER FIRST NAME:");
        final String newFirstName = TerminalUtil.nextLine();
        authenticationService.updateUserFirstName(newFirstName);
        System.out.println("[OK]");
    }

    @Override
    public void updateUserMiddleName() throws Exception {
        System.out.println("[UPDATE USER MIDDLE NAME]");
        System.out.println("ENTER NEW USER MIDDLE NAME:");
        final String newMiddleName = TerminalUtil.nextLine();
        authenticationService.updateUserMiddleName(newMiddleName);
        System.out.println("[OK]");
    }

    @Override
    public void updateUserLastName() throws Exception {
        System.out.println("[UPDATE USER LAST NAME]");
        System.out.println("ENTER NEW USER LAST NAME:");
        final String newLastName = TerminalUtil.nextLine();
        authenticationService.updateUserLastName(newLastName);
        System.out.println("[OK]");
    }

    @Override
    public void updateUserEmail() throws Exception {
        System.out.println("[UPDATE USER EMAIL]");
        System.out.println("ENTER NEW USER EMAIL:");
        final String newEmail = TerminalUtil.nextLine();
        authenticationService.updateUserEmail(newEmail);
        System.out.println("[OK]");
    }

}
