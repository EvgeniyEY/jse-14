package ru.ermolaev.tm.constant;

public interface ArgumentConst {

    String HELP = "-h";

    String ABOUT = "-a";

    String VERSION = "-v";

    String INFO = "-i";

    String COMMANDS = "-cmd";

    String ARGUMENTS = "-arg";

}
