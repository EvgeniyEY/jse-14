package ru.ermolaev.tm.api.controller;

public interface IProjectController {

    void showProjects() throws Exception;

    void clearProjects() throws Exception;

    void createProject() throws Exception;

    void showProjectById() throws Exception;

    void showProjectByIndex() throws Exception;

    void showProjectByName() throws Exception;

    void updateProjectById() throws Exception;

    void updateProjectByIndex() throws Exception;

    void removeProjectById() throws Exception;

    void removeProjectByIndex() throws Exception;

    void removeProjectByName() throws Exception;

}
