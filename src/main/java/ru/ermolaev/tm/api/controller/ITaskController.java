package ru.ermolaev.tm.api.controller;

public interface ITaskController {

    void showTasks() throws Exception;

    void clearTasks() throws Exception;

    void createTask() throws Exception;

    void showTaskById() throws Exception;

    void showTaskByIndex() throws Exception;

    void showTaskByName() throws Exception;

    void updateTaskById() throws Exception;

    void updateTaskByIndex() throws Exception;

    void removeTaskById() throws Exception;

    void removeTaskByIndex() throws Exception;

    void removeTaskByName() throws Exception;

}
