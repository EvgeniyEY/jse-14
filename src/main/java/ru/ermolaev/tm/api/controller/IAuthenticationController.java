package ru.ermolaev.tm.api.controller;

public interface IAuthenticationController {

    void login() throws Exception;

    void logout();

    void registration() throws Exception;

    void updatePassword() throws Exception;

    void showUserProfile() throws Exception;

    void updateUserFirstName() throws Exception;

    void updateUserMiddleName() throws Exception;

    void updateUserLastName() throws Exception;

    void updateUserEmail() throws Exception;

}
