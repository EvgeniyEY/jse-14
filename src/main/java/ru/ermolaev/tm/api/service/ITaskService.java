package ru.ermolaev.tm.api.service;

import ru.ermolaev.tm.model.Task;

import java.util.List;

public interface ITaskService {

    void createTask(String userId, String name) throws Exception;

    void createTask(String userId, String name, String description) throws Exception;

    void addTask(String userId, Task task) throws Exception;

    List<Task> showAllTasks(String userId) throws Exception;

    void removeTask(String userId, Task task) throws Exception;

    void removeAllTasks(String userId) throws Exception;

    Task findTaskById(String userId, String id) throws Exception;

    Task findTaskByIndex(String userId, Integer index) throws Exception;

    Task findTaskByName(String userId, String name) throws Exception;

    Task updateTaskById(String userId, String id, String name, String description) throws Exception;

    Task updateTaskByIndex(String userId, Integer index, String name, String description) throws Exception;

    Task removeTaskById(String userId, String id) throws Exception;

    Task removeTaskByIndex(String userId, Integer index) throws Exception;

    Task removeTaskByName(String userId, String name) throws Exception;

}
