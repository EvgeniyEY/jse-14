package ru.ermolaev.tm.api.service;

import ru.ermolaev.tm.dto.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

    String[] getCommands();

    String[] getArgs();

}
