package ru.ermolaev.tm.dto;

import ru.ermolaev.tm.constant.CommandConst;
import ru.ermolaev.tm.constant.ArgumentConst;

public enum Command implements CommandConst {

    HELP(CommandConst.HELP, ArgumentConst.HELP, "Display terminal commands."),
    ABOUT(CommandConst.ABOUT, ArgumentConst.ABOUT, "Show developer info."),
    VERSION(CommandConst.VERSION, ArgumentConst.VERSION, "Show version info."),
    INFO(CommandConst.INFO, ArgumentConst.INFO, "Show hardware info."),
    EXIT(CommandConst.EXIT, null, "Exit from application."),
    COMMANDS(CommandConst.COMMANDS, ArgumentConst.COMMANDS, "Show application's commands."),
    ARGUMENTS(CommandConst.ARGUMENTS, ArgumentConst.ARGUMENTS, "Show application's arguments."),
    TASK_CREATE(CommandConst.TASK_CREATE, null, "Create a new task."),
    TASK_DELETE(CommandConst.TASK_DELETE, null, "Delete all task."),
    TASK_LIST(CommandConst.TASK_LIST, null, "Show task list."),
    PROJECT_CREATE(CommandConst.PROJECT_CREATE, null, "Create a new project."),
    PROJECT_DELETE(CommandConst.PROJECT_DELETE, null, "Delete all projects."),
    PROJECT_LIST(CommandConst.PROJECT_LIST, null, "Show project list."),
    TASK_SHOW_BY_ID(CommandConst.TASK_SHOW_BY_ID, null, "Show task by id."),
    TASK_SHOW_BY_INDEX(CommandConst.TASK_SHOW_BY_INDEX, null, "Show task by index."),
    TASK_SHOW_BY_NAME(CommandConst.TASK_SHOW_BY_NAME, null, "Show task by name."),
    TASK_UPDATE_BY_ID(CommandConst.TASK_UPDATE_BY_ID, null, "Update task by id."),
    TASK_UPDATE_BY_INDEX(CommandConst.TASK_UPDATE_BY_INDEX, null, "Update task by index."),
    TASK_REMOVE_BY_ID(CommandConst.TASK_REMOVE_BY_ID, null, "Remove task by id."),
    TASK_REMOVE_BY_INDEX(CommandConst.TASK_REMOVE_BY_INDEX, null, "Remove task by index."),
    TASK_REMOVE_BY_NAME(CommandConst.TASK_REMOVE_BY_NAME, null, "Remove task by name."),
    PROJECT_SHOW_BY_ID(CommandConst.PROJECT_SHOW_BY_ID, null, "Show project by id."),
    PROJECT_SHOW_BY_INDEX(CommandConst.PROJECT_SHOW_BY_INDEX, null, "Show project by index."),
    PROJECT_SHOW_BY_NAME(CommandConst.PROJECT_SHOW_BY_NAME, null, "Show project by name."),
    PROJECT_UPDATE_BY_ID(CommandConst.PROJECT_UPDATE_BY_ID, null, "Update project by id."),
    PROJECT_UPDATE_BY_INDEX(CommandConst.PROJECT_UPDATE_BY_INDEX, null, "Update project by index."),
    PROJECT_REMOVE_BY_ID(CommandConst.PROJECT_REMOVE_BY_ID, null, "Remove project by id."),
    PROJECT_REMOVE_BY_INDEX(CommandConst.PROJECT_REMOVE_BY_INDEX, null, "Remove project by index."),
    PROJECT_REMOVE_BY_NAME(CommandConst.PROJECT_REMOVE_BY_NAME, null, "Remove project by name."),
    LOGIN(CommandConst.LOGIN, null, "Login in your account."),
    LOGOUT(CommandConst.LOGOUT, null, "Logout from your account."),
    REGISTRATION(CommandConst.REGISTRATION, null, "Registration of new account."),
    USER_UPDATE_PASSWORD(CommandConst.USER_UPDATE_PASSWORD, null, "Update user password."),
    USER_SHOW_PROFILE(CommandConst.USER_SHOW_PROFILE, null, "Show information about user account."),
    USER_UPDATE_FIRST_NAME(CommandConst.USER_UPDATE_FIRST_NAME, null, "Update user first name."),
    USER_UPDATE_MIDDLE_NAME(CommandConst.USER_UPDATE_MIDDLE_NAME, null, "Update user middle name."),
    USER_UPDATE_LAST_NAME(CommandConst.USER_UPDATE_LAST_NAME, null, "Update user last name."),
    USER_UPDATE_EMAIL(CommandConst.USER_UPDATE_EMAIL, null, "Update user e-mail.");

    private String command = "";

    private String argument = "";

    private String description = "";

    Command(final String command, final String argument, final  String description) {
        this.command = command;
        this.argument = argument;
        this.description = description;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(final String command) {
        this.command = command;
    }

    public String getArgument() {
        return argument;
    }

    public void setArgument(final String argument) {
        this.argument = argument;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        final StringBuilder result = new StringBuilder();
        if (command != null && !command.isEmpty()) result.append(command);
        if (argument != null && !argument.isEmpty()) result.append(", ").append(argument);
        if (description != null && !description.isEmpty()) result.append(": ").append(description);
        return result.toString();
    }

}
