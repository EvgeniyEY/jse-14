package ru.ermolaev.tm.exception.unknown;

public class UnknownIdException extends Exception {

    public UnknownIdException() {
        super("Error! This ID does not exist.");
    }

    public UnknownIdException(final String id) {
        super("Error! This ID [" + id + "] does not exist.");
    }

}
