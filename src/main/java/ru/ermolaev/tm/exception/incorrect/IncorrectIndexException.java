package ru.ermolaev.tm.exception.incorrect;

public class IncorrectIndexException extends Exception {

    public IncorrectIndexException() {
        super("Error! Index is incorrect.");
    }

    public IncorrectIndexException(final String value) {
        super("Error! This value [" + value + "] is not number.");
    }

}
