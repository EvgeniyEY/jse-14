package ru.ermolaev.tm.exception.empty;

public class EmptyLoginException extends Exception {

    public EmptyLoginException() {
        super("Error! Login is empty.");
    }

}
