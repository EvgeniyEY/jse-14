package ru.ermolaev.tm.exception.empty;

public class EmptyLastNameException extends Exception {

    public EmptyLastNameException() {
        super("Error! Last name is empty.");
    }

}
