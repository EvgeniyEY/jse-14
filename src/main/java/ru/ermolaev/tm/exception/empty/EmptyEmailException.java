package ru.ermolaev.tm.exception.empty;

public class EmptyEmailException extends Exception {

    public EmptyEmailException() {
        super("Error! E-mail is empty.");
    }

}
