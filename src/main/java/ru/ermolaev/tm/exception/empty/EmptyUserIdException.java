package ru.ermolaev.tm.exception.empty;

public class EmptyUserIdException extends Exception {

    public EmptyUserIdException() {
        super("Error! User ID is empty.");
    }

}
