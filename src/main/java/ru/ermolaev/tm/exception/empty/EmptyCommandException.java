package ru.ermolaev.tm.exception.empty;

public class EmptyCommandException extends Exception {

    public EmptyCommandException() {
        super("Error! Command is empty.");
    }

}
