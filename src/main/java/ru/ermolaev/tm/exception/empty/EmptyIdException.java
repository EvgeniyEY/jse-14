package ru.ermolaev.tm.exception.empty;

public class EmptyIdException extends Exception {

    public EmptyIdException() {
        super("Error! ID is empty.");
    }

}
