package ru.ermolaev.tm.exception.empty;

public class EmptyPasswordException extends Exception {

    public EmptyPasswordException() {
        super("Error! Password is empty.");
    }

}
