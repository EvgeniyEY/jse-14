package ru.ermolaev.tm.bootstrap;

import ru.ermolaev.tm.api.controller.*;
import ru.ermolaev.tm.api.repository.*;
import ru.ermolaev.tm.api.service.*;
import ru.ermolaev.tm.constant.ArgumentConst;
import ru.ermolaev.tm.constant.CommandConst;
import ru.ermolaev.tm.controller.AuthenticationController;
import ru.ermolaev.tm.controller.CommandController;
import ru.ermolaev.tm.controller.ProjectController;
import ru.ermolaev.tm.controller.TaskController;
import ru.ermolaev.tm.exception.unknown.UnknownArgumentException;
import ru.ermolaev.tm.exception.unknown.UnknownCommandException;
import ru.ermolaev.tm.exception.empty.EmptyCommandException;
import ru.ermolaev.tm.repository.*;
import ru.ermolaev.tm.role.Role;
import ru.ermolaev.tm.service.*;
import ru.ermolaev.tm.util.TerminalUtil;

public class Bootstrap {

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository);

    private final IAuthenticationService authenticationService = new AuthenticationService(userService);

    private final IAuthenticationController authenticationController = new AuthenticationController(authenticationService);

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService, authenticationService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService, authenticationService);

    private void initUsers() throws Exception {
        userService.create("user", "user", "user@test.ru");
        userService.create("admin", "admin", Role.ADMIN);
    }

    public void run(final String[] args) throws Exception {
        System.out.println("Welcome to task manager");
        if (parseArgs(args)) System.exit(0);
        initUsers();
        while (true) {
            try {
                parseCommand(TerminalUtil.nextLine());
            } catch (Exception e) {
                System.err.println(e.getMessage());
                System.err.println("[FAIL]");
            }
        }
    }

    public boolean parseArgs(final String[] args) throws Exception {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

    public void parseArg(final String arg) throws Exception {
        if (arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.INFO:
                commandController.showInfo();
                break;
            case ArgumentConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case ArgumentConst.COMMANDS:
                commandController.showCommands();
                break;
            default:
                throw new UnknownArgumentException(arg);
        }
    }

    public void parseCommand(final String arg) throws Exception {
        if (arg.isEmpty()) throw new EmptyCommandException();
        switch (arg) {
            case CommandConst.HELP:
                commandController.showHelp();
                break;
            case CommandConst.ABOUT:
                commandController.showAbout();
                break;
            case CommandConst.VERSION:
                commandController.showVersion();
                break;
            case CommandConst.INFO:
                commandController.showInfo();
                break;
            case CommandConst.EXIT:
                commandController.exit();
            case CommandConst.COMMANDS:
                commandController.showCommands();
                break;
            case CommandConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case CommandConst.TASK_CREATE:
                taskController.createTask();
                break;
            case CommandConst.TASK_DELETE:
                taskController.clearTasks();
                break;
            case CommandConst.TASK_LIST:
                taskController.showTasks();
                break;
            case CommandConst.PROJECT_CREATE:
                projectController.createProject();
                break;
            case CommandConst.PROJECT_DELETE:
                projectController.clearProjects();
                break;
            case CommandConst.PROJECT_LIST:
                projectController.showProjects();
                break;
            case CommandConst.TASK_SHOW_BY_ID:
                taskController.showTaskById();
                break;
            case CommandConst.TASK_SHOW_BY_INDEX:
                taskController.showTaskByIndex();
                break;
            case CommandConst.TASK_SHOW_BY_NAME:
                taskController.showTaskByName();
                break;
            case CommandConst.TASK_UPDATE_BY_ID:
                taskController.updateTaskById();
                break;
            case CommandConst.TASK_UPDATE_BY_INDEX:
                taskController.updateTaskByIndex();
                break;
            case CommandConst.TASK_REMOVE_BY_ID:
                taskController.removeTaskById();
                break;
            case CommandConst.TASK_REMOVE_BY_INDEX:
                taskController.removeTaskByIndex();
                break;
            case CommandConst.TASK_REMOVE_BY_NAME:
                taskController.removeTaskByName();
                break;
            case CommandConst.PROJECT_SHOW_BY_ID:
                projectController.showProjectById();
                break;
            case CommandConst.PROJECT_SHOW_BY_INDEX:
                projectController.showProjectByIndex();
                break;
            case CommandConst.PROJECT_SHOW_BY_NAME:
                projectController.showProjectByName();
                break;
            case CommandConst.PROJECT_UPDATE_BY_ID:
                projectController.updateProjectById();
                break;
            case CommandConst.PROJECT_UPDATE_BY_INDEX:
                projectController.updateProjectByIndex();
                break;
            case CommandConst.PROJECT_REMOVE_BY_ID:
                projectController.removeProjectById();
                break;
            case CommandConst.PROJECT_REMOVE_BY_INDEX:
                projectController.removeProjectByIndex();
                break;
            case CommandConst.PROJECT_REMOVE_BY_NAME:
                projectController.removeProjectByName();
                break;
            case CommandConst.LOGIN:
                authenticationController.login();
                break;
            case CommandConst.LOGOUT:
                authenticationController.logout();
                break;
            case CommandConst.REGISTRATION:
                authenticationController.registration();
                break;
            case CommandConst.USER_UPDATE_PASSWORD:
                authenticationController.updatePassword();
                break;
            case CommandConst.USER_SHOW_PROFILE:
                authenticationController.showUserProfile();
                break;
            case CommandConst.USER_UPDATE_FIRST_NAME:
                authenticationController.updateUserFirstName();
                break;
            case CommandConst.USER_UPDATE_MIDDLE_NAME:
                authenticationController.updateUserMiddleName();
                break;
            case CommandConst.USER_UPDATE_LAST_NAME:
                authenticationController.updateUserLastName();
                break;
            case CommandConst.USER_UPDATE_EMAIL:
                authenticationController.updateUserEmail();
                break;
            default:
                throw new UnknownCommandException(arg);
        }
    }

}
